#!/bin/bash

# This script trains PLDA models and does scoring.
if [ $# != 8 ]; then
  echo "Usage: $0 <vec-type> <dev-dir> <enroll-dir> <test-dir> <trials-file> <scores-dir> <norm-length> <simple-norm>"
fi

vec_type=$1
dev_dir=$2
enroll_dir=$3
test_dir=$4
trials=$5
score_f=$6
normalize_length=$7    # true | false
simple_length_norm=$8  # true | false

ivector-mean scp:$dev_dir/$vec_type $dev_dir/mean.vec

ivector-compute-plda ark:$dev_dir/spk2utt \
  "ark:ivector-subtract-global-mean $dev_dir/mean.vec scp:$dev_dir/$vec_type ark:- |" \
  $dev_dir/plda

score_dir=`dirname $score_f`
mkdir -p $score_dir

ivector-plda-scoring --normalize-length=$normalize_length \
  --simple-length-normalization=$simple_length_norm \
  --num-utts=ark:$enroll_dir/num_utts.ark \
  "ivector-copy-plda --smoothing=0.0 $dev_dir/plda - |" \
  "ark:ivector-mean ark:$enroll_dir/spk2utt scp:$enroll_dir/$vec_type ark:- | ivector-subtract-global-mean $dev_dir/mean.vec ark:- ark:- |" \
  "ark:ivector-subtract-global-mean $dev_dir/mean.vec scp:$test_dir/$vec_type ark:- |" \
  "cat '$trials' | cut -d\  --fields=1,2 |" $score_f

eer=$(paste $trials $score_f | awk '{print $6, $3}' | compute-eer - 2>/dev/null)
echo "PLDA EER: $eer%"
