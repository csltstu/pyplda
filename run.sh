#!/bin/bash

stage=1

# kaldi to npz
if [ $stage -le 0 ]; then
  local/plda_scoring.sh \
      xvector.scp \
      data/train \
      data/enroll \
      data/test \
      data/test/trials \
      score/plda_scores_kaldi \
      true \
      true
fi


# kaldi to npz
if [ $stage -le 1 ]; then
  for sub in train enroll test; do
    python local/data_io.py \
           --is-eval \
           --src-file data/$sub/xvector.scp \
           --dest-file data/$sub/xvector.npz \
           --utt2spk-file data/$sub/utt2spk
  done
fi


# compute plda
if [ $stage -le 2 ]; then
  python pyplda/ivector_compute_plda.py \
         --train-npz data/train/xvector.npz \
         --global-mean exp/mean.vec \
         --plda exp/plda
fi


# plda scoring
if [ $stage -le 3 ]; then
  python pyplda/ivector_plda_scoring.py \
         --enroll-npz data/enroll/xvector.npz \
         --enroll-num-utts data/enroll/num_utts.ark \
         --test-npz data/test/xvector.npz \
         --trials data/test/trials \
         --global-mean exp/mean.vec \
         --plda exp/plda \
         --plda-dim 512 \
         --score score/plda_scores_python \
         --normalize-length \
         --simple-length-norm
fi
